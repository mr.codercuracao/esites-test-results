# SORRY
**_in between developing this web-app i lost all my project files... i had a test server running in the background i forgot about  so i was able to recover (some) of the files.
sadly this is the compiled version.. it was built with reactjs_**

# To test this project 
2. use any http-server you might have e.g `npm install -g serve`
4. serve this folder

# Notes
> this project was built using [react](https://reactjs.org/)

# Levels
| level         | status        |
| ------------- |:-------------:|
| 1             | completed     |
| 2             | completed     |
| 3             | completed     |
| 4             | completed     |

## level 4 details
here are some of the things i worked on for level 4:
1. dynamic route loading (there's no routes in this app since i was tasked to build only the landing page but the implementation is there)
2. Redux (to **show redux in action** i made the overlays appear via redux)
3. Easy to use lazy loading that wraps over any html element (jsx supported)
4. paralax (background logo)
5. slight styling changes in some places
6. full PWA set-up
7. notification support (no back-end to handle this in this example but it will ask you for permissions)